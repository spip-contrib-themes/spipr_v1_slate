<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bsslate_description' => 'Shades of gunmetal gray',
	'theme_bsslate_slogan' => 'Shades of gunmetal gray',
);
